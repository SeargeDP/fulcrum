package the_fireplace.fulcrum.compat.oldjava;

import java.time.LocalDateTime;

public class NewJava implements IOldCompat {

	@Override
	public String getDate() {
		return LocalDateTime.now().toString();
	}
}
