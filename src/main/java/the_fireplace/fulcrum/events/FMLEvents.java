package the_fireplace.fulcrum.events;

import net.minecraft.util.StatCollector;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.network.FMLNetworkEvent.ClientConnectedToServerEvent;
import the_fireplace.fulcrum.Fulcrum;
import the_fireplace.fulcrum.utils.ChatUtils;
/**
 *
 * @author The_Fireplace
 *
 */
public class FMLEvents {
	@SubscribeEvent
	public void onPlayerJoinClient(final ClientConnectedToServerEvent event) {
		(new Thread() {
			@Override
			public void run() {
				while (FMLClientHandler.instance().getClientPlayerEntity() == null)
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
					}
				Fulcrum.instance.vc.onPlayerJoinClient(FMLClientHandler.instance()
						.getClientPlayerEntity());
				if(Loader.isModLoaded("fireplacecore") && !Loader.isModLoaded("moreclienttweaks")){
					ChatUtils.putMessageInChat(FMLClientHandler.instance().getClientPlayerEntity(), StatCollector.translateToLocal("fulcrum.fc"));
				}
			}
		}).start();
	}

	@SubscribeEvent
	public void onConfigChanged(ConfigChangedEvent.OnConfigChangedEvent eventArgs) {
		if(eventArgs.modID.equals(Fulcrum.MODID))
			Fulcrum.instance.syncConfig();
	}
}
