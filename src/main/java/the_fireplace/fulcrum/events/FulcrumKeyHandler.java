package the_fireplace.fulcrum.events;

import org.lwjgl.input.Keyboard;

import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent.KeyInputEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import the_fireplace.fulcrum.Fulcrum;
import the_fireplace.fulcrum.logger.Logger;
/**
 *
 * @author The_Fireplace
 *
 */
public class FulcrumKeyHandler {
	static final byte UTILITYGUI = 0;
	static final String[] desc = {"key.utilitygui.desc"};
	static final int[] keyValues = {Keyboard.KEY_G};
	final KeyBinding[] keys;
	public FulcrumKeyHandler(){
		keys = new KeyBinding[desc.length];
		for(int i = 0; i < desc.length; ++i){
			keys[i] = new KeyBinding(desc[i], keyValues[i], "key.fulcrum.category");
			ClientRegistry.registerKeyBinding(keys[i]);
		}
	}
	@SideOnly(Side.CLIENT)
	@SubscribeEvent
	public void onKeyInput(KeyInputEvent event){
		if(keys[UTILITYGUI].isPressed()){
			EntityPlayer player = Minecraft.getMinecraft().thePlayer;
			player.openGui(Fulcrum.MODID, UTILITYGUI, player.worldObj, player.getPosition().getX(), player.getPosition().getY(), player.getPosition().getZ());
			Logger.addToLog(Fulcrum.MODID, "Player opened the Utility Gui.");
		}
	}
}
