package the_fireplace.fulcrum.math;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class VersionMath {
	/**
	 * Checks if the second version is higher than the first
	 *
	 * @param firstVersion
	 *            The first version to compare
	 * @param secondVersion
	 *            The second version to compare
	 * @return true if the second version is higher than the first
	 */
	public static boolean isHigherVersion(String firstVersion, String secondVersion) {
		final int[] _current = splitVersion(firstVersion);
		final int[] _new = splitVersion(secondVersion);

		for (int i = 0; i < Math.max(_current.length, _new.length); i++) {
			int curv = 0;
			if (i < _current.length)
				curv = _current[i];

			int newv = 0;
			if (i < _new.length)
				newv = _new[i];

			if (newv > curv) {
				return true;
			} else if (curv > newv) {
				return false;
			} else {
				//go on
			}
		}
		return false;
	}
	/**
	 * Splits a version in its number components (Format ".\d+\.\d+\.\d+.*" )
	 *
	 * @param version
	 *            The version to be splitted (Format is important!
	 * @return The numeric version components as an integer array
	 */
	private static int[] splitVersion(String version) {
		final String[] tmp = version.split("\\.");
		final int size = tmp.length;
		final int out[] = new int[size];

		for (int i = 0; i < size; i++) {
			out[i] = Integer.parseInt(tmp[i]);
		}

		return out;
	}

	public static String getVersionFor(String url){
		String output = "";
		URLConnection con;
		try {
			con = new URL(url).openConnection();

			if (con != null) {
				final BufferedReader br = new BufferedReader(new InputStreamReader(
						con.getInputStream()));

				String input;
				StringBuffer buf = new StringBuffer();
				while ((input = br.readLine()) != null) {
					buf.append(input);
				}
				output = buf.toString();
				br.close();
			}
		} catch (MalformedURLException e) {
			return"0.0.0.0";
		} catch (IOException e) {
			return"0.0.0.0";
		}
		return output;
	}
}
