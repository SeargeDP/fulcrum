package the_fireplace.fulcrum.math;

import java.awt.image.BufferedImage;

public class ImageMath {
	/**
	 * Compares 2 images pixel by pixel.
	 *
	 * @param imgA - the first image
	 * @param imgB - the second image
	 * @return whether the images are the same
	 */
	public static boolean compareImages(BufferedImage imgA, BufferedImage imgB){
		if(imgA.getWidth() == imgB.getWidth() && imgA.getHeight() == imgB.getHeight()){
			int width = imgA.getWidth();
			int height = imgA.getHeight();

			for(int y = 0;y < height; y++){
				for(int x = 0;x < width; x++){
					if(imgA.getRGB(x, y) != imgB.getRGB(x, y)){
						return false;
					}
				}
			}
		}else return false;
		return true;
	}
}
