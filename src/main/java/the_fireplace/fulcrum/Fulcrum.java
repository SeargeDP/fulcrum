package the_fireplace.fulcrum;

import net.minecraft.util.StatCollector;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import the_fireplace.fulcrum.api.API;
import the_fireplace.fulcrum.config.ConfigValues;
import the_fireplace.fulcrum.events.FMLEvents;
import the_fireplace.fulcrum.events.FulcrumKeyHandler;
import the_fireplace.fulcrum.gui.GuiHandler;
import the_fireplace.fulcrum.logger.Logger;
import the_fireplace.fulcrum.math.VersionMath;
import the_fireplace.fulcrum.utils.VersionChecker;
/**
 * @author The_Fireplace
 */
@Mod(modid=Fulcrum.MODID, name=Fulcrum.MODNAME, version=Fulcrum.VERSION, acceptedMinecraftVersions = "1.8", guiFactory = "the_fireplace.fulcrum.config.FulcrumGuiFactory")
public class Fulcrum {
	@Instance(value = Fulcrum.MODID)
	public static Fulcrum instance;
	public static final String MODID = "fulcrum";
	public static final String MODNAME = "Fireplace Utility Library (Core Revamped for Maximum Usability)";
	public static final String VERSION = "2.0.2.0";

	static final String downloadURL = "http://goo.gl/1rhqVT";
	public GuiHandler guiHandler = new GuiHandler();
	public VersionChecker vc;
	public static Logger logger = new Logger(MODID);

	public static Configuration config;
	//Config properties
	public static Property UPDATECHECKER_PROPERTY;

	public void syncConfig(){
		ConfigValues.UPDATECHECKER = UPDATECHECKER_PROPERTY.getBoolean();
	}

	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		logger.create();
		vc = new VersionChecker();
		FMLCommonHandler.instance().bus().register(new FMLEvents());
		if(event.getSide().isClient())
			FMLCommonHandler.instance().bus().register(new FulcrumKeyHandler());
		config = new Configuration(event.getSuggestedConfigurationFile());
		config.load();
		UPDATECHECKER_PROPERTY = config.get(Configuration.CATEGORY_GENERAL, ConfigValues.UPDATECHECKER_NAME, ConfigValues.UPDATECHECKER_DEFAULT);
		UPDATECHECKER_PROPERTY.comment=StatCollector.translateToLocal("UpdateChecker.tooltip");
		syncConfig();
	}

	@EventHandler
	public void init(FMLInitializationEvent event){
		API.registerModToVersionChecker(MODNAME, VERSION, VersionMath.getVersionFor("https://dl.dropboxusercontent.com/s/hwa5d5535luebjy/prerelease.version?dl=0"), VersionMath.getVersionFor("https://dl.dropboxusercontent.com/s/5d9gswpetm66kbg/release.version?dl=0"), downloadURL, MODID);
		NetworkRegistry.INSTANCE.registerGuiHandler(instance, guiHandler);
	}

	@EventHandler
	public void postInit(FMLPostInitializationEvent event){
		if(!Loader.isModLoaded("fireplacecore"))
			Logger.clearOld();
	}
}
