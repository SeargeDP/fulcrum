package the_fireplace.fulcrum.utils;

import java.util.ArrayList;
import java.util.Map;

import com.google.common.collect.Maps;

import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.event.ClickEvent;
import net.minecraft.event.ClickEvent.Action;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ChatStyle;
import net.minecraft.util.EnumChatFormatting;
import net.minecraftforge.fml.common.Loader;
import the_fireplace.fulcrum.Fulcrum;
import the_fireplace.fulcrum.config.ConfigValues;
import the_fireplace.fulcrum.math.VersionMath;
/**
 *
 * @author The_Fireplace
 *
 */
public class VersionChecker {
	private ArrayList modids;
	private final Map modNames = Maps.newHashMap();
	private final Map currVers = Maps.newHashMap();
	private final Map betaVers = Maps.newHashMap();
	private final Map relVers = Maps.newHashMap();
	private final Map mURLs = Maps.newHashMap();

	public VersionChecker(){
		modids = new ArrayList();
	}

	public void addMod(String modid, String modDisplayName, String currentVersion, String newBetaVersion, String newFullVersion, String updateURL){
		Fulcrum.logger.addToLog("Attempting to register "+modDisplayName+"("+modid+") to the version checker.");
		if(!modids.contains(modid)){
			modids.add(modid);
			Fulcrum.logger.addToLog(modDisplayName+"("+modid+") was successfully registered.");
		}else{
			Fulcrum.logger.addToLog("Error: "+modid+" has already been registered.");
			return;
		}
		modNames.put(modid, modDisplayName);
		currVers.put(modid, currentVersion);
		betaVers.put(modid, newBetaVersion);
		relVers.put(modid, newFullVersion);
		mURLs.put(modid, updateURL);
	}

	public void onServerStarted(){
		for(int i=0;i<modids.size();i++){
			notifyServer((String)modids.get(i));
		}
	}

	private void notifyServer(String modid) {
		String VERSION = (String) currVers.get(modid);
		String MODNAME = (String) modNames.get(modid);
		String RELVER = (String) relVers.get(modid);
		String BETAVER = (String) betaVers.get(modid);
		String downloadURL = (String) mURLs.get(modid);
		if(ConfigValues.UPDATECHECKER){
			if (VersionMath.isHigherVersion(VERSION, RELVER) && VersionMath.isHigherVersion(BETAVER, RELVER)) {
				notifyServer(MODNAME, RELVER, downloadURL);
			}else if(VersionMath.isHigherVersion(VERSION, BETAVER)){
				notifyServer(MODNAME, BETAVER, downloadURL);
			}
		}
	}

	private void notifyServer(String modname, String relver, String downloadURL) {
		Fulcrum.logger.addToLog("Notified server of the available update to "+relver);
		System.out.println("Version "+relver+" of "+modname+" is available!");
		System.out.println("Download it at "+downloadURL+" !");
	}

	public void onPlayerJoinClient(EntityPlayer player){
		for(int i=0;i<modids.size();i++){
			notifyClient(player, (String) modids.get(i));
		}
	}

	public void notifyClient(EntityPlayer player, String modid){
		String VERSION = (String) currVers.get(modid);
		String MODNAME = (String) modNames.get(modid);
		String RELVER = (String) relVers.get(modid);
		String BETAVER = (String) betaVers.get(modid);
		String downloadURL = (String) mURLs.get(modid);
		if(MODNAME == Fulcrum.MODNAME)
			MODNAME = "FULCRUM";
		if(ConfigValues.UPDATECHECKER){
			if (VersionMath.isHigherVersion(VERSION, RELVER) && VersionMath.isHigherVersion(BETAVER, RELVER)) {
				notifyClient(player, MODNAME, RELVER, downloadURL);
			}else if(VersionMath.isHigherVersion(VERSION, BETAVER)){
				notifyClient(player, MODNAME, BETAVER, downloadURL);
			}
		}
	}

	private void notifyClient(EntityPlayer player, String modname, String version, String downloadURL) {
		if(!Loader.isModLoaded("VersionChecker")){
			ICommandSender ics = player;
			ics.addChatMessage(new ChatComponentText("A new version of "+modname+" is available!"));
			ics.addChatMessage(new ChatComponentText("=========="+version+"=========="));
			ics.addChatMessage(new ChatComponentText("Get it at the following link:"));
			ics.addChatMessage(new ChatComponentText(downloadURL).setChatStyle(new ChatStyle().setItalic(true).setUnderlined(true).setColor(EnumChatFormatting.BLUE).setChatClickEvent(new ClickEvent(Action.OPEN_URL, downloadURL))));
		}
	}
}
