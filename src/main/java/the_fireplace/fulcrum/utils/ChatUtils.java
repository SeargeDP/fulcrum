package the_fireplace.fulcrum.utils;

import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.event.ClickEvent;
import net.minecraft.event.ClickEvent.Action;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ChatStyle;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ChatUtils {
	/**
	 * Puts a clickable message in the player's chat. Doesn't affect the server.
	 * @param player
	 * 		The player to receive the message
	 * @param message
	 * 		The message to be sent. There is no character limit. It can be split with \n.
	 * @param link
	 * 		The link the message leads to
	 */
	@SideOnly(Side.CLIENT)
	public static void putMessageInChat(EntityPlayer player, String message, String link) {
		String[] lines = message.split("\n");

		for (String line : lines)
			((ICommandSender) player)
			.addChatMessage(new ChatComponentText(line).setChatStyle(new ChatStyle().setChatClickEvent(new ClickEvent(Action.OPEN_URL, link))));
	}
	/**
	 * Puts a message in the player's chat. Doesn't affect the server.
	 * @param player
	 * 		The player to receive the message
	 * @param message
	 * 		The message to be sent. There is no character limit. It can be split with \n.
	 */
	@SideOnly(Side.CLIENT)
	public static void putMessageInChat(EntityPlayer player, String message){
		String[] lines = message.split("\n");

		for (String line : lines)
			((ICommandSender) player)
			.addChatMessage(new ChatComponentText(line));
	}
}
