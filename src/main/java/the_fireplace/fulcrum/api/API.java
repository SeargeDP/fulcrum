package the_fireplace.fulcrum.api;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.event.FMLInterModComms;
import the_fireplace.fulcrum.Fulcrum;
import the_fireplace.fulcrum.math.VersionMath;

public class API {
	/**
	 * Registers update information for Dynious's Version Checker and my version checker
	 * @param modDisplayName
	 *    The name of the mod being registered
	 * @param oldVersion
	 *    The current version of your mod
	 * @param newPreVersion
	 *    The pre-release(beta) version retrieved by the version checker
	 * @param newVersion
	 *    The release version retrieved by the version checker
	 * @param updateURL
	 *    the URL users should go to to get the new version
	 * @param modid
	 *    The modid of the mod being registered
	 */
	public static void registerModToVersionChecker(String modDisplayName, String oldVersion, String newPreVersion, String newVersion, String updateURL, String modid){
		NBTTagCompound updateInfo = new NBTTagCompound();
		String versiontoshow;
		if (!newVersion.equals("") && !newPreVersion.equals("")) {
			if(VersionMath.isHigherVersion(newVersion, newPreVersion)){
				versiontoshow = newPreVersion;
			}else{
				versiontoshow = newVersion;
			}
		}else{
			versiontoshow = "0.0.0.0";
		}
		updateInfo.setString("modDisplayName", modDisplayName);
		updateInfo.setString("oldVersion", oldVersion);
		updateInfo.setString("newVersion", versiontoshow);
		updateInfo.setString("updateURL", updateURL);
		updateInfo.setBoolean("isDirectLink", false);
		if(VersionMath.isHigherVersion(oldVersion, versiontoshow))
			FMLInterModComms.sendRuntimeMessage(modid, "VersionChecker", "addUpdate", updateInfo);

		Fulcrum.instance.vc.addMod(modid, modDisplayName, oldVersion, newPreVersion, newVersion, updateURL);
	}
}
