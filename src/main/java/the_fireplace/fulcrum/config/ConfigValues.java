package the_fireplace.fulcrum.config;


/**
 *
 * @author The_Fireplace
 *
 */
public class ConfigValues {
	public static final boolean UPDATECHECKER_DEFAULT = true;
	public static boolean UPDATECHECKER;
	public static final String UPDATECHECKER_NAME = "UpdateChecker";
}
